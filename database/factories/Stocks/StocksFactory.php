<?php

namespace Database\Factories\Stocks;

use App\Domain\Stocks\Models\Stock;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domain\Stocks\Models\Stock>
 */
class StocksFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Stock::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'store_id' => $this->faker->unique()->randomNumber(3),
            'product_id' => $this->faker->unique()->randomNumber(8),
            'offer_id' => $this->faker->unique()->randomNumber(8),
            'qty' => $this->faker->randomFloat(0, 1, 99),
        ];
    }
}
