<?php

namespace Database\Factories\Offers;

use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\OfferCert;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domain\Offers\Models\OfferCert>
 */
class OfferCertsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OfferCert::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $fileId = $this->faker->numberBetween(100, 500);
        $days = $this->faker->numberBetween(10, 365);

        return [
            'offer_id' => Offer::factory()->create()->id,
            'name' => 'Test',
            'date_end' => Carbon::now()->addDays($days),
            'file' => "path/to/image_{$fileId}.png",
        ];
    }
}
