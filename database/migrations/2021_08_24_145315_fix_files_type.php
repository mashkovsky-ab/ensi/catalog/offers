<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        DB::table('offer_certs')->delete();
        Schema::table('offer_certs', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
        });
    }

    public function down()
    {
        DB::table('offer_certs')->delete();
        DB::statement('alter table offer_certs alter column file type jsonb using file::jsonb');
    }
};
