<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropUnique(['product_id', 'merchant_id']);
            $table->renameColumn('merchant_id', 'seller_id');
            $table->unique(['product_id', 'seller_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropUnique(['product_id', 'seller_id']);
            $table->renameColumn('seller_id', 'merchant_id');
            $table->unique(['product_id', 'merchant_id']);
        });
    }
};
