<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->unsigned();

            $table->integer('merchant_id')->unsigned();
            $table->string('external_id');
            $table->tinyInteger('sale_status')->unsigned()->default(1);
            $table->char('storage_address')->nullable();

            $table->timestamps();

            $table->unique(['product_id', 'merchant_id']);
        });

        Schema::create('offer_certs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('offer_id')->unsigned();
            $table->string('name')->nullable();
            $table->date('date_end')->nullable();
            $table->jsonb('file')->nullable();

            $table->timestamps();

            $table->foreign('offer_id')->references('id')->on('offers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_certs');
        Schema::dropIfExists('offers');
    }
};
