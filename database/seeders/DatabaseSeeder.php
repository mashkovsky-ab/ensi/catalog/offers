<?php

use Illuminate\Database\Seeder;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\OfferCert;
use App\Domain\Stocks\Models\Stock;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Offer::factory()->count(100)->create();
        OfferCert::factory()->count(10)->create();

        Stock::factory()->count(100)->create();
    }
}
