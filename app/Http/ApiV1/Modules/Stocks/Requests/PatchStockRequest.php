<?php

namespace App\Http\ApiV1\Modules\Stocks\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStockRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'store_id' => ['sometimes', 'integer'],
            'product_id' => ['sometimes', 'integer'],
            'offer_id' => ['sometimes', 'integer'],
            'qty' => ['sometimes', 'numeric'],
        ];
    }
}
