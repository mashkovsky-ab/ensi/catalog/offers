<?php


namespace App\Http\ApiV1\Modules\Stocks\Requests;


use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReserveStocksRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'items' => 'required|array',
            'items.*.store_id' => 'required',
            'items.*.offer_id' => 'required',
            'items.*.qty' => 'required',
        ];
    }

    public function getItems(): array
    {
        return $this->get('items');
    }
}
