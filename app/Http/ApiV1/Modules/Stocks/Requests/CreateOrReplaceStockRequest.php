<?php

namespace App\Http\ApiV1\Modules\Stocks\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceStockRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'store_id' => ['required', 'integer'],
            'product_id' => ['required', 'integer'],
            'offer_id' => ['required', 'integer'],
            'qty' => ['required', 'numeric'],
        ];
    }
}
