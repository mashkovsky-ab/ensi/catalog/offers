<?php

namespace App\Http\ApiV1\Modules\Stocks\Queries;

use App\Domain\Stocks\Models\Stock;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StocksQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Stock::query());

        $this->allowedSorts([
            'id',
            'store_id',
            'offer_id',
            'qty',
        ]);

        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('store_id'),
            AllowedFilter::exact('offer_id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::exact('qty'),
            AllowedFilter::scope('created_at_from'),
            AllowedFilter::scope('created_at_to'),
        ]);
    }
}
