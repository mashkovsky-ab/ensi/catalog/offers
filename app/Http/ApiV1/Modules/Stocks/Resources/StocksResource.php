<?php

namespace App\Http\ApiV1\Modules\Stocks\Resources;

use App\Domain\Stocks\Models\Stock;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Stock
 */
class StocksResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'product_id' => $this->product_id,
            'offer_id' => $this->offer_id,
            'qty' => $this->qty,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
