<?php

namespace App\Http\ApiV1\Modules\Stocks\Controllers;


use App\Domain\Stocks\Actions\Stocks\CreateStockAction;
use App\Domain\Stocks\Actions\Stocks\DeleteManyStocksAction;
use App\Domain\Stocks\Actions\Stocks\DeleteStockAction;
use App\Domain\Stocks\Actions\Stocks\PatchStockAction;
use App\Domain\Stocks\Actions\Stocks\ReplaceStockAction;
use App\Domain\Stocks\Actions\Stocks\ReserveStockAction;
use App\Http\ApiV1\Modules\Stocks\Queries\StocksQuery;
use App\Http\ApiV1\Modules\Stocks\Requests\CreateOrReplaceStockRequest;
use App\Http\ApiV1\Modules\Stocks\Requests\PatchStockRequest;
use App\Http\ApiV1\Modules\Stocks\Requests\ReserveStocksRequest;
use App\Http\ApiV1\Modules\Stocks\Resources\StocksResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class StocksController
{
    public function search(PageBuilderFactory $pageBuilderFactory, StocksQuery $query): AnonymousResourceCollection
    {
        return StocksResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $entityId, StocksQuery $query): StocksResource
    {
        return new StocksResource($query->findOrFail($entityId));
    }

    public function create(CreateOrReplaceStockRequest $request, CreateStockAction $action): StocksResource
    {
        return new StocksResource($action->execute($request->validated()));
    }

    public function replace(
        int $entityId,
        CreateOrReplaceStockRequest $request,
        ReplaceStockAction $action
    ): StocksResource {
        return new StocksResource($action->execute($entityId, $request->validated()));
    }

    public function patch(int $entityId, PatchStockRequest $request, PatchStockAction $action): StocksResource
    {
        return new StocksResource($action->execute($entityId, $request->validated()));
    }

    public function delete(int $entityId, DeleteStockAction $action): EmptyResource
    {
        $action->execute($entityId);
        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyStocksAction $action): EmptyResource
    {
        $action->execute($request->getIds());
        return new EmptyResource();
    }

    public function reserve(ReserveStocksRequest $request, ReserveStockAction $action)
    {
        $action->execute($request->getItems());
        return new EmptyResource();
    }

}
