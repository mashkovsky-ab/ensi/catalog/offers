<?php

namespace App\Http\ApiV1\Modules\Offers\Controllers;


use App\Domain\Offers\Actions\Certs\CreateOfferCertAction;
use App\Domain\Offers\Actions\Certs\DeleteOfferCertAction;
use App\Domain\Offers\Actions\Certs\PatchOfferCertAction;
use App\Domain\Offers\Actions\Certs\ReplaceOfferCertAction;
use App\Domain\Offers\Actions\Certs\UploadOfferCertFileAction;
use App\Http\ApiV1\Modules\Offers\Queries\OfferCertsQuery;
use App\Http\ApiV1\Modules\Offers\Requests\CreateOrReplaceOfferCertRequest;
use App\Http\ApiV1\Modules\Offers\Requests\PatchOfferCertRequest;
use App\Http\ApiV1\Modules\Offers\Requests\UploadOfferCertFileRequest;
use App\Http\ApiV1\Modules\Offers\Resources\OfferCertsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OfferCertsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, OfferCertsQuery $query): AnonymousResourceCollection
    {
        return OfferCertsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $entityId, OfferCertsQuery $query): OfferCertsResource
    {
        return new OfferCertsResource($query->findOrFail($entityId));
    }

    public function create(CreateOrReplaceOfferCertRequest $request, CreateOfferCertAction $action): OfferCertsResource
    {
        return new OfferCertsResource($action->execute($request->validated()));
    }

    public function replace(
        int $entityId,
        CreateOrReplaceOfferCertRequest $request,
        ReplaceOfferCertAction $action
    ): OfferCertsResource {
        return new OfferCertsResource($action->execute($entityId, $request->validated()));
    }

    public function upload(
        int $entityId,
        UploadOfferCertFileRequest $request,
        UploadOfferCertFileAction $action
    ): OfferCertsResource {
        return new OfferCertsResource($action->execute($entityId, $request->file('file')));
    }

    public function patch(int $entityId, PatchOfferCertRequest $request, PatchOfferCertAction $action): OfferCertsResource
    {
        return new OfferCertsResource($action->execute($entityId, $request->validated()));
    }

    public function delete(int $entityId, DeleteOfferCertAction $action): EmptyResource
    {
        $action->execute($entityId);
        return new EmptyResource();
    }


}
