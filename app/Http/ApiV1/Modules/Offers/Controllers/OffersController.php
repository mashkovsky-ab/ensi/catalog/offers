<?php

namespace App\Http\ApiV1\Modules\Offers\Controllers;

use App\Domain\Offers\Actions\Offers\CreateOfferAction;
use App\Domain\Offers\Actions\Offers\DeleteManyOffersAction;
use App\Domain\Offers\Actions\Offers\DeleteOfferAction;
use App\Domain\Offers\Actions\Offers\PatchOfferAction;
use App\Domain\Offers\Actions\Offers\ReplaceOfferAction;
use App\Http\ApiV1\Modules\Offers\Queries\OffersQuery;
use App\Http\ApiV1\Modules\Offers\Requests\CreateOrReplaceOfferRequest;
use App\Http\ApiV1\Modules\Offers\Requests\PatchOfferRequest;
use App\Http\ApiV1\Modules\Offers\Resources\OffersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OffersController
{
    public function search(PageBuilderFactory $pageBuilderFactory, OffersQuery $query): AnonymousResourceCollection
    {
        return OffersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $entityId, OffersQuery $query): OffersResource
    {
        return new OffersResource($query->findOrFail($entityId));
    }

    public function create(CreateOrReplaceOfferRequest $request, CreateOfferAction $action): OffersResource
    {
        return new OffersResource($action->execute($request->validated()));
    }

    public function replace(
        int $entityId,
        CreateOrReplaceOfferRequest $request,
        ReplaceOfferAction $action
    ): OffersResource {
        return new OffersResource($action->execute($entityId, $request->validated()));
    }

    public function patch(int $entityId, PatchOfferRequest $request, PatchOfferAction $action): OffersResource
    {
        return new OffersResource($action->execute($entityId, $request->validated()));
    }

    public function delete(int $entityId, DeleteOfferAction $action): EmptyResource
    {
        $action->execute($entityId);

        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyOffersAction $action): EmptyResource
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }
}
