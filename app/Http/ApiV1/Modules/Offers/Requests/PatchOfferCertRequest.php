<?php

namespace App\Http\ApiV1\Modules\Offers\Requests;

use App\Domain\Offers\Models\Offer;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchOfferCertRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'offer_id' => ['sometimes', Rule::exists(Offer::class, 'id')],
            'name' => ['sometimes', 'string'],
            'date_end' => ['sometimes', 'date'],
        ];
    }
}
