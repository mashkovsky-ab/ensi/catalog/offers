<?php

namespace App\Http\ApiV1\Modules\Offers\Requests;

use App\Domain\Offers\Models\Offer;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceOfferCertRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'offer_id' => ['required', Rule::exists(Offer::class, 'id')],
            'name' => ['required', 'string'],
            'date_end' => ['nullable', 'date'],
        ];
    }
}
