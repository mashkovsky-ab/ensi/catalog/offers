<?php

namespace App\Http\ApiV1\Modules\Offers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UploadOfferCertFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }
}
