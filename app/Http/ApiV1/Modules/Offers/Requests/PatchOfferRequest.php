<?php

namespace App\Http\ApiV1\Modules\Offers\Requests;

use App\Domain\Offers\Enums\SaleStatus;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchOfferRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'product_id' => ['sometimes', 'integer'],
            'seller_id' => ['sometimes', 'integer'],
            'sale_status' => ['sometimes', 'integer', new Enum(SaleStatus::class)],
            'external_id' => ['sometimes', 'nullable', 'string'],
            'storage_address' => ['sometimes', 'nullable', 'string'],
            'base_price' => ['sometimes', 'nullable', 'integer'],
        ];
    }
}
