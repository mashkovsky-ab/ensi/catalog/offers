<?php

namespace App\Http\ApiV1\Modules\Offers\Requests;

use App\Domain\Offers\Enums\SaleStatus;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CreateOrReplaceOfferRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'product_id' => ['required', 'integer'],
            'seller_id' => ['required', 'integer'],
            'sale_status' => ['required', 'integer', new Enum(SaleStatus::class)],
            'external_id' => ['nullable', 'string'],
            'storage_address' => ['nullable', 'string'],
            'base_price' => ['nullable', 'integer'],
        ];
    }
}
