<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Models\Offer;
use App\Http\ApiV1\Modules\Stocks\Resources\StocksResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Offer
 */
class OffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'seller_id' => $this->seller_id,
            'external_id' => $this->external_id,
            'sale_status' => $this->sale_status,
            'storage_address' => $this->storage_address,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'base_price' => $this->base_price,
            'stocks' => StocksResource::collection($this->whenLoaded('stocks')),
        ];
    }
}
