<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Models\OfferCert;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin OfferCert
 */
class OfferCertsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'offer_id' => $this->offer_id,
            'name' => $this->name,
            'date_end' => $this->date_end,
            'file' => $this->mapPublicFileToResponse($this->file),
        ];
    }
}
