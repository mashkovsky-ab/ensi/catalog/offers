<?php

namespace App\Http\ApiV1\Modules\Offers\Queries;

use App\Domain\Offers\Models\Offer;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OffersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Offer::query());

        $this->allowedIncludes(['stocks']);
        $this->allowedSorts(['id', 'product_id', 'seller_id', 'external_id', 'sale_status', 'storage_address']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::exact('seller_id'),
            AllowedFilter::exact('sale_status'),
            AllowedFilter::scope('created_at_gte', 'createdAtFrom'),
            AllowedFilter::scope('created_at_lte', 'createdAtTo'),
        ]);
    }
}
