<?php

namespace App\Http\ApiV1\Modules\Offers\Queries;

use App\Domain\Offers\Models\OfferCert;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OfferCertsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(OfferCert::query());

        $this->allowedSorts(['id', 'name', 'date_end']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('date_end'),
            AllowedFilter::scope('created_at_from'),
            AllowedFilter::scope('created_at_to'),
            AllowedFilter::scope('date_end_from'),
            AllowedFilter::scope('date_end_to'),
        ]);
    }
}
