<?php

use App\Domain\Offers\Models\Offer;
use App\Http\ApiV1\Modules\Offers\Tests\Factories\OfferFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\travel;

uses(ApiV1ComponentTestCase::class)->group('offers', 'component');

test('POST /api/v1/offers/offers only required fields success', function () {
    $request = OfferFactory::new()->onlyRequired()->make(['external_id' => null]);

    $id = postJson('/api/v1/offers/offers', $request)
        ->assertCreated()
        ->assertJsonFragment($request)
        ->json('data.id');

    assertDatabaseHas(Offer::tableName(), ['id' => $id]);
});

test('POST /api/v1/offers/offers success', function () {
    $request = OfferFactory::new()->make();

    $id = postJson('/api/v1/offers/offers', $request)
        ->assertCreated()
        ->assertJsonFragment($request)
        ->json('data.id');

    assertDatabaseHas(Offer::tableName(), ['id' => $id]);
});

test('PATCH /api/v1/offers/offers', function () {
    $offer = Offer::factory()->createOne();
    $request = OfferFactory::new()
        ->except(['product_id', 'seller_id'])
        ->make(['base_price' => $offer->base_price]);

    patchJson("/api/v1/offers/offers/{$offer->id}", $request)
        ->assertOk()
        ->assertJsonPath('data.base_price', $request['base_price']);

    assertDatabaseHas(
        $offer->getTable(),
        ['product_id' => $offer->product_id, 'seller_id' => $offer->seller_id, 'base_price' => $request['base_price']]
    );
});

test('POST /api/v1/offers/offers:search success', function () {
    $offers = Offer::factory()->count(3)->create(['seller_id' => 0]);

    /** @var Offer $offer */
    $offer = $offers->last();
    $request = [
        'filter' => ['product_id' => $offer->product_id, 'seller_id' => 0],
    ];

    postJson('/api/v1/offers/offers:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonStructure(['data' => [['id', 'product_id', 'seller_id', 'base_price']]])
        ->assertJsonPath('data.0.id', $offers[2]->id);
});

test('POST /api/v1/offers/offers:search for multiple products success', function () {
    $offers = Offer::factory()->count(3)->create(['seller_id' => 0])->take(2);
    $request = [
        'filter' => ['product_id' => $offers->pluck('product_id')->all()],
    ];

    postJson('/api/v1/offers/offers:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonPath('data.0.id', $offers[0]->id);
});

test('POST /api/v1/offers/offers:search with created_at filter', function () {
    $oldOffer = Offer::factory()->createOne();

    travel(20)->minutes();
    $expectedOffer = Offer::factory()->createOne();

    $request = [
        'filter' => ['created_at_gte' => $oldOffer->created_at->addMinutes(2)],
    ];

    postJson('/api/v1/offers/offers:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $expectedOffer->id);
});

test('GET /api/v1/offers/offers/{id} success', function () {
    $offer = Offer::factory()->createOne();

    getJson("/api/v1/offers/offers/{$offer->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $offer->id);
});
