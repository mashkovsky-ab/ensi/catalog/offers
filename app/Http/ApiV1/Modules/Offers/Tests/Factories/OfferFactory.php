<?php

namespace App\Http\ApiV1\Modules\Offers\Tests\Factories;

use App\Domain\Offers\Enums\SaleStatus;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class OfferFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'product_id' => $this->faker->unique()->randomNumber(8),
            'seller_id' => $this->faker->unique()->randomNumber(8),
            'external_id' => $this->faker->unique()->bothify('###??###'),
            'sale_status' => $this->faker->randomElement(SaleStatus::cases())->value,
            'storage_address' => $this->faker->optional()->address,
            'base_price' => $this->faker->numberBetween(10, 100_000),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function onlyRequired(): self
    {
        return $this->only(['product_id', 'seller_id', 'sale_status']);
    }
}
