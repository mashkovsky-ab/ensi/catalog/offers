<?php

namespace App\Http\ApiV1\Support\Resources;

use App\Domain\Support\Models\Model;
use Illuminate\Http\Resources\MissingValue;

/**
 * @property string $name
 * @property string|null $code
 *
 * @mixin Model
 */
class EntityReferenceResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->getKey(),
            'name' => $this->name,
            'code' => $this->getAttributeValue('code') ?? new MissingValue(),
        ];
    }
}
