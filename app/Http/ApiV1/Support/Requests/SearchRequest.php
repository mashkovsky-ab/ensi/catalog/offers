<?php

namespace App\Http\ApiV1\Support\Requests;

class SearchRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'filter' => ['nullable', 'array'],
            'include' => ['nullable', 'array'],
            'include.*' => ['string'],
            'sort' => ['nullable', 'array'],
            'sort.*' => ['string'],
            'pagination' => ['nullable', 'array'],
            'pagination.offset' => ['nullable', 'integer'],
            'pagination.limit' => ['nullable', 'integer'],
        ];
    }
}
