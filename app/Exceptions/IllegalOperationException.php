<?php

namespace App\Exceptions;

use LogicException;

class IllegalOperationException extends LogicException
{
    //
}
