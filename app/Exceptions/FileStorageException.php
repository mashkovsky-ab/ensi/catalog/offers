<?php

namespace App\Exceptions;

use RuntimeException;

class FileStorageException extends RuntimeException
{
    //
}
