<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\Model;
use RuntimeException;
use Throwable;

class ConstraintViolationException extends RuntimeException
{
    const INTEGRITY = '23000';
    const RESTRICT = '23001';
    const NOT_NULL = '23502';
    const FOREIGN_KEY = '23503';
    const UNIQUE = '23505';
    const CHECK = '23514';

    /** @var Model|null */
    private $model;

    public function __construct(?Model $model, string $message = '', $code = self::INTEGRITY, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);

        $this->code = $code;
        $this->model = $model;
    }

    public function getModel(): ?Model
    {
        return $this->model;
    }

    public function isUniqueViolation(): bool
    {
        return $this->getCode() === self::UNIQUE;
    }

    public function isNotNullViolation(): bool
    {
        return $this->getCode() === self::NOT_NULL;
    }

    public static function codes(): array
    {
        return [
            self::INTEGRITY,
            self::RESTRICT,
            self::NOT_NULL,
            self::FOREIGN_KEY,
            self::UNIQUE,
            self::CHECK,
        ];
    }
}
