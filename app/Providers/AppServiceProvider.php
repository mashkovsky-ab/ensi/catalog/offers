<?php

namespace App\Providers;

use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\OfferCert;
use App\Domain\Offers\Observers\OfferCertObserver;
use App\Domain\Offers\Observers\OfferObserver;
use App\Support\Files\FileStorage;
use Carbon\CarbonImmutable;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Contracts\Foundation\Application;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\DateFactory;
use Illuminate\Support\ServiceProvider;
use Storage;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        DateFactory::useClass(CarbonImmutable::class);

        $this->app->singleton(FileStorage::class, function (Application $app) {
            return new FileStorage(Storage::disk($app->make(EnsiFilesystemManager::class)->publicDiskName()));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        $this->addObservers();
    }

    protected function addObservers(): void
    {
        Offer::observe(OfferObserver::class);
        OfferCert::observe(OfferCertObserver::class);
    }
}
