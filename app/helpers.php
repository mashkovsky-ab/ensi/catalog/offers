<?php


if (!function_exists('in_production')) {

    /**
     * Находится ли приложение в прод режиме
     * @return bool
     */
    function in_production(): bool
    {
        return app()->environment('production');
    }
}

if (!function_exists('data_combine_assoc')) {

    /**
     * Создает массив с ключами $keys. Значения ищутся по ключу в массиве $values.
     * Если значение не найдено, то подставляется $defaultValue.
     *
     * @param array $keys
     * @param array $values
     * @param mixed|null $defaultValue
     * @return array
     */
    function data_combine_assoc(array $keys, array $values, mixed $defaultValue = null): array
    {
        return array_replace(
            array_fill_keys($keys, $defaultValue),
            Arr::only($values, $keys)
        );
    }
}
