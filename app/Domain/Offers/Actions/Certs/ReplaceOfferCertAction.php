<?php

namespace App\Domain\Offers\Actions\Certs;

use App\Domain\Offers\Models\OfferCert;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class ReplaceOfferCertAction
{
    use AppliesToAggregate;

    public function execute(int $offerCertId, array $fields): OfferCert
    {
        return $this->updateOrCreate($offerCertId, function (OfferCert $offerCertId) use ($fields) {
            $offerCertId->fill(
                data_combine_assoc($offerCertId->getFillable(), $fields)
            );
        });
    }

    protected function createModel(): Model
    {
        return new OfferCert();
    }
}
