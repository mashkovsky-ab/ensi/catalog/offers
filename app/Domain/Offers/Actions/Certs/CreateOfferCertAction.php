<?php

namespace App\Domain\Offers\Actions\Certs;

use App\Domain\Offers\Models\OfferCert;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class CreateOfferCertAction
{
    use AppliesToAggregate;

    public function execute(array $fields): OfferCert
    {
        return $this->updateOrCreate(null, function (OfferCert $offerCert) use ($fields) {
            $offerCert->fill($fields);
        });
    }

    protected function createModel(): Model
    {
        return new OfferCert();
    }
}
