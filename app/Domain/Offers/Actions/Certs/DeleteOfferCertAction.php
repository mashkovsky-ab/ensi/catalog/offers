<?php

namespace App\Domain\Offers\Actions\Certs;

use App\Domain\Offers\Models\OfferCert;
use App\Domain\Support\Concerns\AppliesToAggregate;

class DeleteOfferCertAction
{
    use AppliesToAggregate;

    public function execute(int $offerCertId): void
    {
        $this->delete($offerCertId);
    }

    protected function createModel(): OfferCert
    {
        return new OfferCert();
    }
}
