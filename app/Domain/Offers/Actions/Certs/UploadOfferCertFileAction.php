<?php

namespace App\Domain\Offers\Actions\Certs;

use App\Domain\Offers\Models\OfferCert;
use App\Domain\Support\Actions\BaseFileAction;
use App\Domain\Support\Models\Model;
use App\Support\Files\FileIntent;
use Illuminate\Http\UploadedFile;

class UploadOfferCertFileAction extends BaseFileAction
{
    public function execute(int $offerCertId, ?UploadedFile $file): OfferCert
    {
        return $this->transaction(function () use ($offerCertId, $file) {
            return $this->attach($offerCertId, $this->createIntent($offerCertId, $file), 'file');
        });
    }

    protected function createIntent(int $offerCertId, UploadedFile $file): FileIntent
    {
        return FileIntent::create($file)
            ->inDirectory('certs')
            ->withNamePrefix("cert_$offerCertId");
    }

    protected function createModel(): Model
    {
        return new OfferCert();
    }
}
