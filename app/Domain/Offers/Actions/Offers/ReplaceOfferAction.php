<?php

namespace App\Domain\Offers\Actions\Offers;

use App\Domain\Offers\Models\Offer;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class ReplaceOfferAction
{
    use AppliesToAggregate;

    public function execute(int $offerId, array $fields): Offer
    {
        return $this->updateOrCreate($offerId, function (Offer $offer) use ($fields) {
            $offer->fill(
                data_combine_assoc($offer->getFillable(), $fields)
            );
        });
    }

    protected function createModel(): Model
    {
        return new Offer();
    }
}
