<?php

namespace App\Domain\Offers\Actions\Offers;

use App\Domain\Offers\Models\Offer;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class DeleteOfferAction
{
    use AppliesToAggregate;

    public function execute(int $offerId): void
    {
        $this->delete($offerId);
    }

    protected function createModel(): Model
    {
        return new Offer();
    }
}
