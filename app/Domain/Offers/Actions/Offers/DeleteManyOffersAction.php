<?php

namespace App\Domain\Offers\Actions\Offers;

use App\Domain\Support\Concerns\HandlesMassOperation;

class DeleteManyOffersAction
{
    use HandlesMassOperation;

    public function __construct(private DeleteOfferAction $deleteAction)
    {
    }

    public function execute(array $offerIds): void
    {
        $this->each($offerIds, function (int $offerIds) {
            $this->deleteAction->execute($offerIds);
        });
    }
}
