<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Models\OfferCert;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Storage;

class OfferCertObserver
{
    public function __construct(private EnsiFilesystemManager $filesystemManager)
    {
    }

    public function deleted(OfferCert $cert)
    {
        $this->deleteFile($cert->file);
    }

    protected function deleteFile(?string $path)
    {
        $disk = Storage::disk($this->filesystemManager->publicDiskName());
        if ($path) {
            $disk->delete($path);
        }
    }
}
