<?php

namespace App\Domain\Offers\Observers;

//use App\Core\Elastic\IndexFactory;
//use App\Core\Elastic\ScheduleRunner;
use App\Domain\Offers\Models\Offer;

class OfferObserver
{
    protected $scheduler;

//    public function __construct(ScheduleRunner $scheduler)
//    {
//        $this->scheduler = $scheduler;
//    }

    /**
     * Handle the model "created" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function created(Offer $offer)
    {
//        $this->scheduler->markForIndex(IndexFactory::TYPE_PRODUCT, $offer->product_id);
    }

    /**
     * Handle the model "updated" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function updated(Offer $offer)
    {
//        $this->scheduler->markForIndex(IndexFactory::TYPE_PRODUCT, $offer->product_id);
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param Offer $offer
     * @return void
     */
    public function deleted(Offer $offer)
    {
//        $this->scheduler->markForIndex(IndexFactory::TYPE_PRODUCT, $offer->product_id);
    }
}
