<?php

namespace App\Domain\Offers\Enums;

enum SaleStatus : int
{
    case ON_SALE = 1;
    case OUT_SALE = 2;
}
