<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\OfferFactory;
use App\Domain\Stocks\Models\Stock;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class Offer
 * @package App\Domain\Offers\Models
 *
 * @property int $product_id - id товара
 * @property int $seller_id - id продавца
 * @property string $external_id - id товара у продавца
 * @property int $sale_status - статус продажи
 * @property string $storage_address - адрес хранения товара в магазине (для сборщика)
 * @property int|null $base_price - базовая цена
 *
 * @property-read Collection|OfferCert[] $certs
 * @property-read Collection|Stock[] $stocks
 */
class Offer extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'seller_id',
        'external_id',
        'sale_status',
        'storage_address',
        'base_price',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Factories\Factory<\App\Domain\Offers\Models\Offer>|OfferFactory
     */
    public static function factory(): OfferFactory
    {
        return OfferFactory::new();
    }

    public function certs(): HasMany
    {
        return $this->hasMany(OfferCert::class);
    }

    public function stocks(): HasMany
    {
        return $this->hasMany(Stock::class, 'offer_id');
    }

    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }
}
