<?php

namespace App\Domain\Offers\Models;

use App\Domain\Support\Models\Model;
use Carbon\Carbon;
use Database\Factories\Offers\OfferCertsFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OfferCert
 * @package App\Domain\Offers\Models
 *
 * @property int $offer_id
 * @property string $name
 * @property Carbon|null $date_end
 * @property string|null $file
 *
 * @property-read Offer $offer
 */
class OfferCert extends Model
{
    use HasFactory;

    protected $fillable = [
        'offer_id',
        'name',
        'date_end',
        'file',
    ];

    /**
     * @var array<string, string>
     */
    protected $casts = [
        'date_end' => 'datetime',
    ];

    public static function factory(): OfferCertsFactory
    {
        return OfferCertsFactory::new();
    }

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }

    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }

    public function scopeDateEndFrom(Builder $query, $date): Builder
    {
        return $query->where('date_end', '>=', $date);
    }

    public function scopeDateEndTo(Builder $query, $date): Builder
    {
        return $query->where('date_end', '<=', $date);
    }
}
