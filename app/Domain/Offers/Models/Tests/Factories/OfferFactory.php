<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Enums\SaleStatus;
use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Factories\Factory;

class OfferFactory extends Factory
{
    protected $model = Offer::class;

    public function definition(): array
    {
        return [
            'product_id' => $this->faker->unique()->randomNumber(8),
            'seller_id' => $this->faker->unique()->randomNumber(8),
            'external_id' => $this->faker->unique()->bothify('###??###'),
            'sale_status' => $this->faker->randomElement(SaleStatus::cases()),
            'storage_address' => $this->faker->optional()->address,
            'base_price' => $this->faker->numberBetween(10, 100_000),
        ];
    }
}
