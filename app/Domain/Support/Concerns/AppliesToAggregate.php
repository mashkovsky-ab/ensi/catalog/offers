<?php

namespace App\Domain\Support\Concerns;

use App\Domain\Support\Models\Model;
use Closure;
use Ensi\LaravelAuditing\Facades\Transaction;
use Illuminate\Support\Facades\DB;

trait AppliesToAggregate
{
    use InteractsWithModels;

    /**
     * Создает новую модель корневой сущности.
     *
     * @return Model
     */
    abstract protected function createModel(): Model;

    protected function lock(int $entityId): Model
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->createModel()
            ->newQuery()
            ->lockForUpdate()
            ->findOrFail($entityId);
    }

    protected function apply(int $entityId, Closure $action): mixed
    {
        $wrapper = function () use ($entityId, $action) {
            $model = $this->lock($entityId);

            Transaction::setRootEntity($model);

            return $action($model);
        };

        return $this->transaction($wrapper);
    }

    protected function updateOrCreate(?int $entityId, Closure $action): mixed
    {
        $wrapper = function () use ($entityId, $action) {
            $model = $entityId === null
                ? $this->createModel()
                : $this->lock($entityId);

            Transaction::setRootEntity($model);

            $action($model);

            if (!$model->exists || $model->isDirty()) {
                $this->saveOrThrow($model);
            }

            return $model;
        };

        return $this->transaction($wrapper);
    }

    protected function delete(int $entityId, ?Closure $action = null): void
    {
        $wrapper = function () use ($entityId, $action) {
            $model = $this->lock($entityId);

            Transaction::setRootEntity($model);

            if ($action !== null) {
                $action($model);
            }

            $this->deleteOrThrow($model);
        };

        $this->transaction($wrapper);
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    protected function transaction(Closure $action)
    {
        return Transaction::isActive()
            ? $action()
            : DB::transaction($action);
    }
}
