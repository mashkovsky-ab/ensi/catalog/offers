<?php

namespace App\Domain\Support\Concerns;

use App\Exceptions\MassOperationException;
use Closure;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

trait HandlesMassOperation
{
    public function each(array $ids, Closure $handler): void
    {
        $errors = [];

        foreach ($ids as $entityId) {
            try {
                $handler($entityId);
            } catch (ModelNotFoundException) {
                // Игнорируем
            } catch (Exception $exception) {
                $errors[$entityId] = $exception->getMessage();
            }
        }

        if (!empty($errors)) {
            throw new MassOperationException($errors);
        }
    }
}
