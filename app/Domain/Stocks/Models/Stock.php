<?php

namespace App\Domain\Stocks\Models;

use App\Domain\Offers\Models\Offer;
use App\Domain\Support\Models\Model;
use Database\Factories\Stocks\StocksFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Остатки товаров на складах (запасы)"
 * Class Stock
 * @package App\Domain\Stocks\Models
 *
 * @property int $store_id - id склада
 * @property int $product_id - id товара
 * @property int $offer_id - id предложения продавца
 * @property float $qty - кол-во товара
 *
 * @property-read Offer $offer
 */
class Stock extends Model
{
    use HasFactory;

    protected $fillable = [
        'store_id',
        'product_id',
        'offer_id',
        'qty',
    ];

    public static function factory(): StocksFactory
    {
        return StocksFactory::new();
    }

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }

    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }

}
