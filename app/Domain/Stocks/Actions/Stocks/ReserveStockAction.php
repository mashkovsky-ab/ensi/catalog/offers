<?php


namespace App\Domain\Stocks\Actions\Stocks;


use App\Domain\Stocks\Models\Stock;
use Illuminate\Database\Eloquent\Collection;

class ReserveStockAction
{
    public function execute(array $items): void
    {
        $items = collect($items);

        /** @var Collection|Stock[] $stocks */
        $stocks = Stock::query()
            ->whereIn('store_id', $items->pluck('store_id')->unique()->all())
            ->whereIn('offer_id', $items->pluck('offer_id')->unique()->all())
            ->get();

        $assocStocks = [];
        foreach ($stocks as $stock) {
            if (!isset($assocStocks[$stock->store_id])) {
                $assocStocks[$stock->store_id] = [];
            }

            $assocStocks[$stock->store_id][$stock->offer_id] = $stock;
        }

        foreach ($items as $item) {
            /** @var Stock|null $stock */
            $stock = null;
            if (isset($assocStocks[$item['store_id']])) {
                if (isset($assocStocks[$item['store_id']][$item['offer_id']])) {
                    $stock = $assocStocks[$item['store_id']][$item['offer_id']];
                }
            }

            if (!$stock) {
                continue;
            }

            $stock->qty = max(0, $stock->qty - $item['qty']);
            $stock->save();
        }
    }
}