<?php

namespace App\Domain\Stocks\Actions\Stocks;

use App\Domain\Stocks\Models\Stock;
use App\Domain\Support\Concerns\AppliesToAggregate;

class DeleteStockAction
{
    use AppliesToAggregate;

    public function execute(int $stockId): void
    {
        $this->delete($stockId);
    }

    protected function createModel(): Stock
    {
        return new Stock();
    }
}
