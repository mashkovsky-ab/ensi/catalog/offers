<?php

namespace App\Domain\Stocks\Actions\Stocks;

use App\Domain\Support\Concerns\HandlesMassOperation;

class DeleteManyStocksAction
{
    use HandlesMassOperation;

    public function __construct(private DeleteStockAction $deleteAction)
    {
    }

    public function execute(array $stockIds): void
    {
        $this->each($stockIds, function (int $stockIds) {
            $this->deleteAction->execute($stockIds);
        });
    }
}
