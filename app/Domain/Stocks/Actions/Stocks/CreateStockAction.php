<?php

namespace App\Domain\Stocks\Actions\Stocks;

use App\Domain\Stocks\Models\Stock;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class CreateStockAction
{
    use AppliesToAggregate;

    public function execute(array $fields): Stock
    {
        return $this->updateOrCreate(null, function (Stock $stock) use ($fields) {
            $stock->fill($fields);
        });
    }

    protected function createModel(): Model
    {
        return new Stock();
    }
}
