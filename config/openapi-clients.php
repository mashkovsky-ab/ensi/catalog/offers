<?php

return [
   'catalog' => [
      'pim' => [
         'base_uri' => env('PIM_SERVICE_HOST') . "/api/v1",
      ],
   ],
   'units' => [
      'bu' => [
         'base_uri' => env('BU_SERVICE_HOST') . "/api/v1",
      ],
   ],
];
